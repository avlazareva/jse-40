package ru.t1.lazareva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.enumerated.Sort;
import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    @NotNull
    Project add(@NotNull Project model) throws Exception;

    @NotNull
    Project add(@Nullable String userId, @NotNull Project model) throws Exception;

    @NotNull
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) throws Exception;

    @NotNull
    Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) throws Exception;

    @NotNull
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    @NotNull
    Project updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) throws Exception;

    void clear(@Nullable String userId) throws Exception;

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception;

    @NotNull
    Project create(@Nullable String userId, @Nullable String name) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    @Nullable
    List<Project> findAll(@Nullable String userId) throws Exception;

    @Nullable
    List<Project> findAll(@Nullable String userId, @Nullable Comparator comparator) throws Exception;

    @Nullable
    List<Project> findAll(@Nullable String userId, @Nullable Sort sort) throws Exception;

    @Nullable
    Project findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    Project findOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    void remove(@Nullable String userId, @Nullable Project model) throws Exception;

    void removeById(@Nullable String userId, @Nullable String id) throws Exception;

    void removeByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

}